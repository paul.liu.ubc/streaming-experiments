#include <bits/stdc++.h>

using namespace std;

const int N = 100;
const int C = 12;
const int INF = 0x3f3f3f3f;

unordered_map<int, int> memo[N][N]; 
int dp(int x, int y, int k) {
	if (x == 0 && y == 0) return 0;
	if (k < 0) return INF;
	if (memo[x][y][k]) return memo[x][y][k];
	int& res = memo[x][y][k] = INF;

	// Increasing the first coordinate (c1, c2, c3, c4)
	if (x <= y+1 && x > 0) {
		res = min(res, 1 + dp(x-1, y, k-1));
		res = min(res, 1 + dp(0, y, k-1) + dp(x-1, 0, C));
	}
	if (x > y+1 && x > 0) {
		res = min(res, 1 + dp(x-1, y, k-1) + dp(0, x-1, C));
		res = min(res, 1 + dp(0, y, k-1) + dp(x-1, x-1, C));
	}

	// Increasing the second coordinate (c5, c6, c7, c8)
	if (y >= x+1 && y > 0) {
		res = min(res, 1 + dp(x, y-1, k-1) + dp(y, 0, C));
		res = min(res, 1 + dp(x, 0, k-1) + dp(y, y-1, C));
	}
	if (y < x+1 && y > 0) {
		res = min(res, 1 + dp(x, y-1, k-1));
		res = min(res, 1 + dp(x, 0, k-1) + dp(0, y-1, C));
	}
	return res;
}

map<vector<int>, int> memo_nd[2 * N];
int sum(const vector<int>& x) {
	int res = 0;
	for (int a : x) res += a;
	return res;
}

int dp_nd(vector<int> x, int k) {
	if (sum(x) == 0) return 0;
	if (k < 0) return INF;
	if (memo_nd[k][x]) return memo_nd[k][x];
	int& res = memo_nd[k][x] = INF;

	for (int i = 0; i < (int) x.size(); i++) {
		if (x[i] == 0) continue;
		// Increasing ith coordinate requires checking
		// that its <= x[j]+1 for j > i and < x[j]+1 for j < i
		for (int j = 0; j < (int) x.size(); j++) {
			if ((x[i] < x[j]+1 && j < i) || (x[i] <= x[j] + 1 && j > i)) {
				auto x_ = x, x__ = x;
				x_[i] = x[i] - 1;
				res = min(res, 1 + dp_nd(x_, k-1));

				x_[i] = 0;
				x__[j] = 0;
				x__[i] = x[i] - 1;
				res = min(res, 1 + dp_nd(x_, k-1) + dp_nd(x__, C));
			}

			if (x[i] >= x[j]+1 && j < i) {
				auto x_ = x, x__ = x;
				x_[i] = x[i] - 1;
				x__[i] = 0;
				x__[j] = x[i];
				res = min(res, 1 + dp_nd(x_, k-1) + dp_nd(x__, C));

				x_[i] = 0;
				x__[i] = x[i] - 1;
				x__[j] = x[i];
				res = min(res, 1 + dp_nd(x_, k-1) + dp_nd(x__, C));
			}

			if (x[i] > x[j] + 1 && j > i) {
				auto x_ = x, x__ = x;
				x_[i] = x[i] - 1;
				x__[i] = 0;
				x__[j] = x[i] - 1;
				res = min(res, 1 + dp_nd(x_, k-1) + dp_nd(x__, C));

				x_[i] = 0;
				x__[i] = x[i] - 1;
				x__[j] = x[i] - 1;
				res = min(res, 1 + dp_nd(x_, k-1) + dp_nd(x__, C));
			}
		}
	}
	return res;
}

int main() {
	for (int i = 1; i < 2 * C; i++) {
		double result = dp(i, i, C+1) / double(C);
		cerr << i << " " << result << endl;//" " << dp_nd(vector<int>({i, i, i}), C+1) / double(C) << endl;
		if (result > 2*exp(1)) break;
	}
	return 0;
}