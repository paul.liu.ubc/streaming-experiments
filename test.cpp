#include <bits/stdc++.h>

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>

#include "graph.h"

using namespace std;
using namespace graph_lib;

void check_coloring(vector<int>& col, vector<full_edge>& edges) {
	map<int, set<int>> colset;
	int i = 0;
	for (auto e : edges) {
		assert(!colset[e.src].count(col[i]));
		assert(!colset[e.dst].count(col[i]));
		colset[e.src].insert(col[i]);
		colset[e.dst].insert(col[i]);
		i++;
	}
	cerr << "Successful check!" << endl;
}

int streaming_edge_colour_max_fast(vector<full_edge>& edges) {
	unordered_map<int, int> maxc;
	int col = 0;
	for (const auto& e : edges) {
		int C = max(maxc[e.src], maxc[e.dst]);
		maxc[e.src] = maxc[e.dst] = C + 1;
		col = max(col, C + 1);
	}
	return col;
}

vector<int> streaming_edge_colour_max(vector<full_edge>& edges) {
	vector<int> col;
	map<int, int> maxc;
	for (auto e : edges) {
		int C = max(maxc[e.src], maxc[e.dst]);
		maxc[e.src] = maxc[e.dst] = C + 1;
		col.push_back(C);
	}
	return col;
}

vector<int> streaming_edge_colour_max_min_random_local(vector<full_edge>& edges) {
	vector<int> col;
	map<int, int> maxc, minc;
	int M = 0, m = 0;
	for (auto e : edges) {
		int Cu = max(maxc[e.src], maxc[e.dst]);
		int Cl = min(minc[e.src], minc[e.dst]);
		int du = Cu - min(maxc[e.src], maxc[e.dst]) + 1;
		int dl = max(minc[e.src], minc[e.dst]) - Cl + 1;
		if (rand() % (du + dl) >= dl) {
			minc[e.src] = minc[e.dst] = Cl - 1;
			col.push_back(Cl - 1);
			m = min(m, Cl - 1);
		} else {
			maxc[e.src] = maxc[e.dst] = Cu + 1;
			col.push_back(Cu);
			M = max(M, Cu + 1);
		}
	}
	int above = 0, below = 0, zero = 0;
	for (int c : col) {
		above += (c > 0);
		below += (c < 0);
		zero += (c == 0);
	}
	cerr << above << " " << below << " " << zero << " " << double(above) / double(above + below) << endl;
	cerr << "Max and min colours used: " << M << " " << m << endl;
	return col;
}

vector<int> streaming_edge_colour_max_min_random_global(vector<full_edge>& edges) {
	vector<int> col;
	map<int, int> maxc, minc;
	int M = 0, m = 0;
	for (auto e : edges) {
		int Cu = max(maxc[e.src], maxc[e.dst]);
		int Cl = min(minc[e.src], minc[e.dst]);
		int du = M - min(maxc[e.src], maxc[e.dst]) + 1;
		int dl = max(minc[e.src], minc[e.dst]) - m + 1;
		if (rand() % (du + dl) < dl) {
			minc[e.src] = minc[e.dst] = Cl - 1;
			col.push_back(Cl - 1);
			m = min(m, Cl - 1);
		} else {
			maxc[e.src] = maxc[e.dst] = Cu + 1;
			col.push_back(Cu);
			M = max(M, Cu + 1);
		}
	}
	int above = 0, below = 0, zero = 0;
	for (int c : col) {
		above += (c > 0);
		below += (c < 0);
		zero += (c == 0);
	}
	cerr << above << " " << below << " " << zero << " " << double(above) / double(above + below) << endl;
	cerr << "Max and min colours used: " << M << " " << m << endl;
	return col;
}

vector<int> streaming_edge_colour_max_min_deterministic(vector<full_edge>& edges) {
	vector<int> col;
	map<int, int> maxc, minc;
	for (auto e : edges) {
		int Cu = max(maxc[e.src], maxc[e.dst]);
		int Cl = min(minc[e.src], minc[e.dst]);
		int du = Cu - min(maxc[e.src], maxc[e.dst]);
		int dl = max(minc[e.src], minc[e.dst]) - Cl;

		if (-Cl < Cu) {
			first:
			minc[e.src] = minc[e.dst] = Cl - 1;
			col.push_back(Cl - 1);
		} else if (-Cl > Cu) {
			second:
			maxc[e.src] = maxc[e.dst] = Cu + 1;
			col.push_back(Cu);
		} else {
			if (du > dl) {
				goto first;
			} else if (dl > du) {
				goto second;
			}

			if (rand() % 2) {
				goto first;
			} else {
				goto second;
			}
		}
	}
	/*
	int above = 0, below = 0, zero = 0;
	for (int c : col) {
		above += (c > 0);
		below += (c < 0);
		zero += (c == 0);
	}
	cerr << above << " " << below << " " << zero << " " << double(above) / double(above + below) << endl;
	*/
	return col;
}

// How about a gap filling algorithm?
// Have a intervals of colours from [1, b] for each vertex.
// When an edge comes in, pick an integer [1, a] and on interval a do the max counter algorithm.

// Intuition: instead of positive and negative, its much better to think of this as
// a random walk with some sort of "teleports" on an nD lattice, where D is the number
// of gaps or intervals allowed. The default we have is just num gaps = 2.

// Instead of minimizing the movement of an interval, we can try to minimize the max
// norm of the vector! This provides some crazy performance??!?!

// TODO: How does our algorithm perform on trees???
vector<int> streaming_edge_colour_gap_filling_deterministic(vector<full_edge>& edges, int n, int NUM_GAPS = 10) {
	vector<int> col;
	vector<vector<int>> gapmax(n);
	for (int i = 0; i < n; i++) {
		gapmax[i].resize(NUM_GAPS);
	}

	int c = 0;
	map<pair<int, int>, int> colmap;
	vector<int> deltas(NUM_GAPS), limits(NUM_GAPS);
	for (auto e : edges) {
		int min_g = 0, min_del = n;
		for (int g = 0; g < NUM_GAPS; g++) {
			int Cu = max(gapmax[e.src][g], gapmax[e.dst][g]);
			int du = Cu - min(gapmax[e.src][g], gapmax[e.dst][g]) + 1;
			deltas[g] = du;
			limits[g] = Cu;
			// TODO: INVESTIGATE THE EFFECTS OF PUTTING Cu vs du here
			if (Cu < min_del) {
				min_g = g;
				min_del = Cu;
			}
		}
		// Look for the smallest delta and increase it, or do it probabilistically
		gapmax[e.src][min_g] = gapmax[e.dst][min_g] = limits[min_g] + 1;
		auto index = make_pair(min_g, limits[min_g]);
		if (!colmap.count(index)) {
			colmap[index] = c++;	
		}
		col.push_back(colmap[index]);
	}
	return col;
}

vector<int> streaming_edge_colour_gap_filling_randomized(vector<full_edge>& edges, int n, int NUM_GAPS = 10) {
	vector<int> col;
	vector<vector<int>> gapmax(n);
	for (int i = 0; i < n; i++) {
		gapmax[i].resize(NUM_GAPS);
	}

	int c = 0;
	map<pair<int, int>, int> colmap;
	vector<int> limits(NUM_GAPS);
	vector<double> weight(NUM_GAPS);
	for (auto e : edges) {
		double sum = 0;
		for (int g = 0; g < NUM_GAPS; g++) {
			int Cu = max(gapmax[e.src][g], gapmax[e.dst][g]);
			limits[g] = Cu;
			
			weight[g] = 1. / pow(1. + Cu, 2);
			sum += weight[g];
		}

		// Look for the smallest delta and increase it, or do it probabilistically
		int g = 0;
		double r = ( rand() % (1<<30) )* 1./ (1<<30) * sum;
		while (true) {
			r -= weight[g];
			if (r < 0) {
				gapmax[e.src][g] = gapmax[e.dst][g] = limits[g] + 1;
				auto index = make_pair(g, limits[g]);
				if (!colmap.count(index)) {
					colmap[index] = c++;	
				}
				col.push_back(colmap[index]);
				break;
			}
			g++;
		}
	}
	return col;
}

vector<int> streaming_edge_colour_gap_filling_uniform(vector<full_edge>& edges, int n, int NUM_GAPS = 10) {
	vector<int> col;
	vector<vector<int>> gapmax(n);
	for (int i = 0; i < n; i++) {
		gapmax[i].resize(NUM_GAPS);
	}

	int c = 0;
	map<pair<int, int>, int> colmap;
	for (auto e : edges) {
		// Look for the smallest delta and increase it, or do it probabilistically
		int g = rand() % NUM_GAPS;
		int maxc = max(gapmax[e.src][g], gapmax[e.dst][g]);
		gapmax[e.src][g] = gapmax[e.dst][g] = maxc + 1;
		auto index = make_pair(g, maxc);
		if (!colmap.count(index)) {
			colmap[index] = c++;	
		}
		col.push_back(colmap[index]);
	}
	return col;
}

vector<full_edge> streaming_edge_colour_gap_filling_adversarial(int n, int NUM_GAPS = 10) {
	vector<int> col;
	vector<vector<int>> gapmax(n);
	for (int i = 0; i < n; i++) {
		gapmax[i].resize(NUM_GAPS);
	}

	map<int, map<int, bool>> added;
	vector<full_edge> result;
	for (int ec = 0; ec < n*(n-1)/2; ec++) {
		// Greedily find a bad edge to add in
		set<pair<pair<int, int>, full_edge>> q;
		for (int i = 0; i < n; i++) {
			for (int j = i+1; j < n; j++) {
				if (!added[i][j]) {
					// See if adding this edge increases the maximum gap or meets it
					pair<int, int> min_del(n, n);
					for (int g = 0; g < NUM_GAPS; g++) {
						int Cu = max(gapmax[i][g], gapmax[j][g]);
						int du = 2 * Cu - gapmax[i][g] - gapmax[j][g];
						// TODO: INVESTIGATE THE EFFECTS OF PUTTING Cu vs du here
						if (make_pair(Cu, du) < min_del) {
							min_del = make_pair(Cu, du);
						}
					}

					q.insert({min_del, full_edge{i, j, 1}});
					if (q.size() > 3) q.erase(q.begin());
				}
			}
		}

		auto e = (--q.end())->second;
		result.push_back(e);
		added[e.src][e.dst] = true;

		int min_g = 0, min_del = n;
		for (int g = 0; g < NUM_GAPS; g++) {
			int Cu = max(gapmax[e.src][g], gapmax[e.dst][g]);
			// TODO: INVESTIGATE THE EFFECTS OF PUTTING Cu vs du here
			if (Cu < min_del) {
				min_g = g;
				min_del = Cu;
			}
		}
		// Look for the smallest delta and increase it, or do it probabilistically
		gapmax[e.src][min_g] = gapmax[e.dst][min_g] = min_del + 1;
	}
	return result;
}

#define PERMUTE_EDGES 0
#define PERMUTE_VERTICES 0
#define GENERATE_ADVERSARIAL 0
#define GENERATE_PLOTS 0

int main(int argc, char* argv[]) {
	extern char *optarg;
	static char usage[] = "usage: %s [-d nverts] [-t nverts] [-c nverts] [-b nverts] [-f fname] [-e extra_params]\none of -d (d-regular mode) -t (tree mode), -b (bipartite chain mode), -c (clique mode), or -f (graph file mode) is required\n";

	int flags = 0, c, n = 0, K = 2;
	string filename;
	while ((c = getopt(argc, argv, "c:f:t:b:e:d:")) != -1) {
		switch (c) {
			case 'c':
				flags = 1;
				n = atoi(optarg);
				break;
			case 'f':
				flags = 2;
				filename = string(optarg);
				break;
			case 't':
				flags = 3;
				n = atoi(optarg);
				break;
			case 'b':
				flags = 4;
				n = atoi(optarg);
				break;
			case 'd':
				flags = 5;
				n = atoi(optarg);
				break;
			case 'e':
				K = atoi(optarg);
				break;
			case '?':
				break;
		}
	}

	if (flags < 1) {	/* missing mandatory options */
		fprintf(stderr, "%s: missing options\n", argv[0]);
		fprintf(stderr, usage, argv[0]);
		exit(1);
	}

	int maxd = 0;
	vector<full_edge> edges;
	if (flags == 2) {
		Graph G = read_graph(filename.c_str());
		n = G.size();
		for (int i = 0; i < n; i++) {
			maxd = max(maxd, (int) G[i].size());
		}
		edges = extract_edges(G);
	} else if (flags == 3) {
		for (int i = 0; i < n; i++) {
			if (i / 2 != i) {
				int u = i / 2, v = i;
				for (int u_ = K * u; u_ < K * u + K; u_++) {
					for (int v_ = K * v; v_ < K * v + K; v_++) {
						edges.push_back({u_, v_, 0});
					}
				}
			}
		}
		maxd = 3 * K;
		n = n * K;
	} else if (flags == 4) {
		n = (n / K) * K;
		for (int i = K; i < n; i++) {
			int v = i;
			int u0 = ((i-K) / K) * K;
			for (int d = 0; d < K; d++) {
				int u = d + u0;
				edges.push_back({u, v, 0});
			}
		}
		maxd = 2 * K;
	} else if (flags == 5) {
		// d-regular graph
		// map<int, int> deg;
		// vector<unordered_set<int>> done(n);
		K = min(K, n / 2);
		for (int u = 0; u < n; u++) {
			for (int j = 0; j < K; j++) {
				int v = (u + j + 1) % n;
				// if (u == v) continue;
				// if (done[v].count(u)) continue;
				
				// deg[u]++;
				// deg[v]++;
				// done[u].insert(v);
				// done[v].insert(u);

				edges.push_back({u, v, 0});
			}
		}
		maxd = 2 * K;
		// for (int i = 0; i < n; i++) {
		// 	maxd = max(maxd, deg[i]);
		// }
	} else {
		for (int i = 0; i < n; i++) {
			for (int j = i+1; j < n; j++) {
				edges.push_back({i, j, 0});
			}
		}
		maxd = n-1;
	}
	cerr << "Finished creating graph..." << endl;
	cerr << "Number of (vertices, edges, max_deg): " << n << " " << edges.size() << " " << maxd << endl;
#if PERMUTE_EDGES
	// Random experiments with 10 edges
	//if (n > 5) return 0;

	double max_r = 0;
	do {
		auto c = streaming_edge_colour_max_min_deterministic(edges);
		double r = (1.0 * set<int>(c.begin(), c.end()).size()) / maxd;
		if (r > max_r) {
			cerr << "New permutation max reached!" << endl;
			max_r = r;
			for (auto e : edges) {
				cerr << e.src << " " << e.dst << endl;
			}
		}
	} while (next_permutation(edges.begin(), edges.end()));
	cout << "Worst-case performance of generalized algorithm (local deterministic 2D - minimizing max norm): " << max_r << endl;
	return 0;
#endif

#if PERMUTE_VERTICES
	// Random experiments with 10 edges
	//if (n > 10) return 0;

	vector<int> index;
	for (int i = 0; i < n; i++) {
		index.push_back(i);
	}

	double max_r = 0;
	do {
		edges.clear();
		for (int i = 0; i < n; i++) {
			for (auto e : G[index[i]]) {
				if (index[i] < e.dst) {
					edges.push_back({index[i], e.dst, e.wt});
				}
			}
		}

		auto c = streaming_edge_colour_max_min_deterministic(edges);
		double r = (1.0 * set<int>(c.begin(), c.end()).size()) / maxd;
		if (r > max_r) {
			cerr << "New permutation max reached! " << r << endl;
			max_r = r;
			/*
			for (auto e : edges) {
				cerr << e.src << " " << e.dst << endl;
			}*/
		}
	} while (next_permutation(index.begin(), index.end()));
	cout << "Worst-case performance of generalized algorithm (local deterministic 2D - minimizing max norm): " << max_r << endl;
	return 0;
#endif

#if GENERATE_ADVERSARIAL
	edges = streaming_edge_colour_gap_filling_adversarial(n, 10);
	// vector<int> perm(n);
	// iota(perm.begin(), perm.end(), 0);
	// random_shuffle(perm.begin(), perm.end());
	// for (auto& e : edges) {
	// 	e.src = perm[e.src];
	// 	e.dst = perm[e.dst];
	// }
	assert(edges.size() == n * (n-1) / 2);
	//for (auto e : edges) cout << e.src << " " << e.dst << endl;
#else
	srand(time(nullptr));
	cerr << "Shuffling edges..." << endl;
	// random_shuffle(edges.begin(), edges.end());
	shuffle(edges.begin(), edges.end(), mt19937(time(0)));
	cerr << "Done Shuffling!" << endl;
#endif
	auto nc = streaming_edge_colour_max_fast(edges);
	cout << "Performance of algorithm 1 (baseline): " << (1.0 * nc) / maxd << endl;
	return 0;

	auto c1 = streaming_edge_colour_max(edges);
	auto c2 = streaming_edge_colour_max_min_random_local(edges);
	auto c3 = streaming_edge_colour_max_min_random_global(edges);
	auto c4 = streaming_edge_colour_max_min_deterministic(edges);
	
	/*
	check_coloring(c1, edges);
	check_coloring(c2, edges);
	check_coloring(c3, edges);
	check_coloring(c4, edges);
	check_coloring(c5, edges);
	*/

	cout << "Performance of algorithm 1 (baseline): " << (1.0 * set<int>(c1.begin(), c1.end()).size()) / maxd << endl;
	cout << "Performance of algorithm 2 (local random 2D - minimizing movement): " << (1.0 * set<int>(c2.begin(), c2.end()).size()) / maxd << endl;
	cout << "Performance of algorithm 3 (global random 2D - minimizing movement): " << (1.0 * set<int>(c3.begin(), c3.end()).size()) / maxd << endl;
	cout << "Performance of algorithm 4 (local deterministic 2D - minimizing max norm): " << (1.0 * set<int>(c4.begin(), c4.end()).size()) / maxd << endl;
	cout << endl;

	// Benchmarking generalized algorithm
	vector<int> dimensions({2, 3, 5, int(log(n)), int(sqrt(n))});
	for (int d : dimensions) {
		auto c = streaming_edge_colour_gap_filling_deterministic(edges, n, d);
		cout << "Performance of generalized algorithm (local deterministic nD - minimizing max norm, k = " << d  << "): " << (1.0 * set<int>(c.begin(), c.end()).size()) / maxd << endl;

		auto c_ = streaming_edge_colour_gap_filling_randomized(edges, n, d);
		cout << "Performance of generalized algorithm (local randomized nD - minimizing max norm, k = " << d  << "): " << (1.0 * set<int>(c_.begin(), c_.end()).size()) / maxd << endl;
	
		auto c__ = streaming_edge_colour_gap_filling_uniform(edges, n, d);
		cout << "Performance of generalized algorithm (uniform randomized nD - minimizing max norm, k = " << d  << "): " << (1.0 * set<int>(c__.begin(), c__.end()).size()) / maxd << endl;
		cout << endl;
	}

#if GENERATE_PLOTS
	// For some reason log log(# colours used / \Delta) = m * log (# counters / Delta) + c
	// So this means # colours used / Delta = exp(C * (# counters / Delta)^p)
	// So if we want # colours used / Delta to converge to a constant, we need
	// C * (# counters / Delta)^p going to a constant. p ~ -1 from experiments.
	// I.e. want C Delta / #counters going to a constant.
	// So for fixed number of counters, it seems like it'll go to infinity.
	// unless #counters = O(Delta), which is not what we want at all.
	// Seems like numerical analysis is a failure here, since we already know
	// that for 1 counter it does go to a constant.

	auto file = ofstream("deterministic-k-counter.dat", ofstream::out);
	//file << "# deterministic-k-counter.dat" << endl;
	//file << "# X Y" << endl;

	cerr << "Creating plot data..." << endl;
	for (int d = 1; d <= n; d++) {
		if (d % 10 == 0) {
			cerr << int(d * 100.0 / n + 1e-6) << " percent completed..." << endl;
		}
		auto c_ = streaming_edge_colour_gap_filling_deterministic(edges, n, d);
		file << d << " " << (1.0 * set<int>(c_.begin(), c_.end()).size()) / maxd << endl;
	}
#endif

	return 0;
}