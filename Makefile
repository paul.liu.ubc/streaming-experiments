test: graph.h test.cpp
	g++ -std=c++11 -Wall -O3 -o test test.cpp

2d-bound: graph.h 2d-bound.cpp
	g++ -std=c++11 -Wall -O3 -o 2d-bound 2d-bound.cpp

all: test 2d-bound

clean:
	rm test
