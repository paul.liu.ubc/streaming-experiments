This repository contains experiments for testing a O(1)-memory streaming algorithm for edge colouring. Inspired by Streaming and Massively Parallel Algorithms for Edge Coloring (Behnezhad et al. 2019).

To compile, simply type `make all`.

Description of functions / experiments (test.cpp)
=================================================

*check_colouring:* 
Checks if colouring is valid.

*streaming_edge_colour_max:* 
Implementation of Behnezhad et al.

- Performance: Approaches 4\Delta

*streaming_edge_colour_max_min_random_local:*
Maintains a positive counter Cu and a negative counter Cl. 
When a new edge comes, we compute how far the counter will increase in the positive direction vs the negative direction.
We then probabilistically choose to either decrement Cl or increment Cu, with higher probability towards the choice that moves the counter the least (in absolute value).

- Performance: Approaches 3.5\Delta

*streaming_edge_colour_max_min_random_global:*
Maintains a positive counter Cu and a negative counter Cl. 
When a new edge comes, we compute how far the counter will increase in the positive direction vs the negative direction *with respect to the maximum positive and negative counter in the entire graph*.
We then probabilistically choose to either decrement Cl or increment Cu, with higher probability towards the choice that moves the counter the least (in absolute value).

- Performance: Approaches >3.5\Delta

*streaming_edge_colour_max_min_deterministic:*
Non-probabilistic version of *streaming_edge_colour_max_min_random_local*.

- Performance: Approaches 2\Delta.

*streaming_edge_colour_gap_filling_deterministic:*
Maintains k counters of the 1D variety. When a new edge comes in, picks the counter that increments the least.

- Performance: Approaches 2\Delta. Surprisingly: If I break ties randomly, it converges a bit above 2\Delta, but if I do it deterministically it converges a bit below 2\Delta. But if we further break ties by choosing the counter that increments the least, it goes below 2\Delta again.

*streaming_edge_colour_gap_filling_randomized:*
Maintains k counters of the 1D variety. When a new edge comes in, compute all k counter increments Ci, and picks probabilistically a counter that increments the least (according to some function on the Ci's. e.g. weight the probablities by 1 / Ci so big increments are penalized).

- Performance: Depends on weight function used. Generally worse than deterministic.

*streaming_edge_colour_gap_filling_uniform:*
Maintains k counters of the 1D variety. When a new edge comes in, pick a random counter to increment.

- Performance: Same or worse than single counter.

*streaming_edge_colour_gap_filling_adversarial:*
Maintains k counters of the 1D variety and simulates the *streaming_edge_colour_gap_filling_deterministic* algorithm. It then adds in edges greedily, trying to increment globally maximum counter (or create more counters that match the globally maximum value).

- Performance: Exhibits bad examples for both the deterministic and the uniform version. Have not tested randomized version.
